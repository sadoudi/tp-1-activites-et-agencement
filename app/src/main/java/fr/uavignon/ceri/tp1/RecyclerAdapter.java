package fr.uavignon.ceri.tp1;

import android.content.Context;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

;
import fr.uavignon.ceri.tp1.data.Country;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String uri = Country.countries[position].getImgUri();
        holder.countryName.setText(Country.countries[position].getName());
        holder.countryCapital.setText(Country.countries[position].getCapital());
        Context c = holder.countryFlag.getContext();
        holder.countryFlag.setImageDrawable(c.getResources().getDrawable(c.getResources().getIdentifier(uri , null, c.getPackageName())));
    }

    @Override
    public int getItemCount() {
        return Country.countries.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView countryFlag;
        TextView countryName;
        TextView countryCapital;

        public ViewHolder(View itemView) {
            super(itemView);
            countryFlag    = itemView.findViewById(R.id.country_flag);
            countryName    = itemView.findViewById(R.id.country_name);
            countryCapital = itemView.findViewById(R.id.country_capital);

            int position = getAdapterPosition();

            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();

                    ListFragmentDirections.ActionListFragmentToDetailFragment action = ListFragmentDirections.actionListFragmentToDetailFragment();
                    action.setCountryId(position);
                    Navigation.findNavController(v).navigate(action);

                }
            });
        }
    }
}
