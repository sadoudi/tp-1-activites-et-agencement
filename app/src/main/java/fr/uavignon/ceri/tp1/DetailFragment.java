package fr.uavignon.ceri.tp1;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import fr.uavignon.ceri.tp1.data.Country;

public class DetailFragment extends Fragment {

    TextView  countryName;
    TextView  countryCapital;
    ImageView countryFlag;
    TextView  countryLanguage;
    TextView  countryCurrency;
    TextView  countryPopulation;
    TextView  countryArea;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        countryName       = view.findViewById(R.id.country_name);
        countryFlag       = view.findViewById(R.id.country_flag);
        countryCapital    = view.findViewById(R.id.country_capital);
        countryLanguage   = view.findViewById(R.id.country_language);
        countryCurrency   = view.findViewById(R.id.country_currency);
        countryPopulation = view.findViewById(R.id.country_population);
        countryArea       = view.findViewById(R.id.country_area);

        countryCapital.setEnabled(false);
        countryLanguage.setEnabled(false);
        countryCurrency.setEnabled(false);
        countryPopulation.setEnabled(false);
        countryArea.setEnabled(false);



        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());

        String uri = Country.countries[args.getCountryId()].getImgUri();
        Context c = countryFlag.getContext();
        countryFlag.setImageDrawable(c.getResources().getDrawable(c.getResources().getIdentifier(uri, null, c.getPackageName())));
        countryName.setText(Country.countries[args.getCountryId()].getName());
        countryCapital.setText(Country.countries[args.getCountryId()].getCapital());
        countryLanguage.setText(Country.countries[args.getCountryId()].getLanguage());
        countryCurrency.setText(Country.countries[args.getCountryId()].getCurrency());
        countryPopulation.setText(Country.countries[args.getCountryId()].getPopulation() + "");
        countryArea.setText(Country.countries[args.getCountryId()].getArea() + " km²");



        view.findViewById(R.id.retour_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(DetailFragment.this)
                        .navigate(R.id.action_DetailFragment_to_ListFragment);
            }
        });
    }
}